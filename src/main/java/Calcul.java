package activite;
/**
* la classe Calcul .
* @author joseph
**/
public final class Calcul {
    /**
    * Constructeur private pour empêcher son instanciation.
    */
    public Calcul() { }
    /**
    * Calcul la somme de deux nombres.
    * @param a entier
    * @param b entier
    * @return la somme
    **/
    public static int somme(final int a, final int b) {
      return a + b;
    }
    /**
    * Divise a par b si b est supérieure ou égale à 10 .
    * @param a entier
    * @param b entier
    * @return a/b si b >= 10 si non b
    */
    public static int maFonction(final int a, final int b) {
      final int n = 10;
      if (b >= n) {
        return a / b;
      }
      return b;
    }
    /**
      * @param a entier
      * @param b entier
      * @return a / b si b != 0
      * @throw IllegalArgumentException si b == 0
      */
    public static int division(final int a, final int b) {
      if (b == 0) {
        throw new IllegalArgumentException("b ne doit pas etre 0");
      }
      return a / b;
    }
}

