import org.junit.Assert;
import org.junit.Test;
import activite.*;
/**
* Tests unitaire pour la classe Calcul.
* @author joseph
**/
public class CalculTest {
    /**
    * Test du constructeur de la classe Calcul.
    */
    @Test
    public void testConstructeur() {
        new Calcul();
    }
    /**
    * Test de la methode somme().
    */
    @Test
    public void testSomme() {
        final int a = 2;
        final int b = 3;
        final int result = 5;
        Assert.assertEquals(result, Calcul.somme(a, b));
    }
    /**
    * Test de la methode division.
    */
    @Test
    public void testDivision() {
        final int a = 8;
        final int b = 2;
        final int result = 4;
        Assert.assertEquals(result, Calcul.division(a, b));
    }
}
